"""
Copyright 2023 Nasser Darwish
Institute of Science and Technology Austria
GNU AFFERO GENERAL PUBLIC LICENSE, Version 3
The python server for an NIS TCP/IP client
"""

import socket # for socket
import sys 
import ctypes
from time import sleep

from _thread import *
import threading

from ariaFlags import Messages
from ariaFlags import SktStr # No use yet

from Aria.SDK import Instrument, Error, StepList
from Aria.SDK import Monitoring, FlowControl, Configuration, Sequence, Enums
from Aria.SDK import ErrorSeverity, FlowRatePreset, SignalType

def main():

    # Strings must be sent with a '\r' to the client
    SKT_MSG_RECEIVED = "socket_msg_rcvd"
    SKT_SRV_RDY      = "socket_srv_rdy"
    SKT_KILL         = "socket_stop"
    SRV_END          = "server_stopped"
        
    ipAddr     = str(sys.argv[1]);
    clientPort = int(sys.argv[2]);
    
    print('Fluidic Server: \n' + ipAddr+ '/' + str(clientPort));
    #Connection loop

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind((ipAddr, clientPort))
    s.listen(1)
    
    # Accept waits until the client connects:
    conn, addr = s.accept();    
    print(conn)
    
    print("Connection from: " + str(addr))
    
    while True:
        data = conn.recv(256);
        if not data:
            sleep(0.001);
        if data!=b'\r\x00':
            # To be understood from the NIS side (NKSocket), 
            # we need this encoding and \r line termination char
            nis_msg = data.decode('utf-16-le')
            if  nis_msg == SKT_KILL:
                # Close request
                conn.send((packer(SRV_END)+'\r').encode('utf-16-le'))
                break;
            else:
                serverReply = parseAndExecute(nis_msg)

                print("serverReply:")
                print(packer(str(serverReply)))

                conn.send((packer(str(serverReply))+'\r').encode('utf-16-le'))
    conn.close();
    print("Ending...")
    exit()

def parseAndExecute(message):    
    command, cmdArgs = parser(message)    
    parseAndExecuteReply = fluCommand(command, cmdArgs)

    print("parseAndExecute:")
    print(parseAndExecuteReply)

    return parseAndExecuteReply    



def fluCommand(command, cmdArgs):

    print("command: "+command)
    print("cmdArgs: "+str(cmdArgs))

    argStr = ""

    if len(cmdArgs)>0:
        for argInd in range(len(cmdArgs)-1):
            argStr = argStr + str(cmdArgs[argInd]) + ','    
        argStr = argStr + str(cmdArgs[len(cmdArgs)])
    
    print(command + "(" + str(argStr) + ")")
    commandOutput = commandSelect(command, cmdArgs)
    return commandOutput

def parser(message):
    command = ""
    cmdArgs = []
    if message.startswith(Messages.ariaHead):
        content = message.split(Messages.ariaHead)[-1].split(Messages.ariaTail)[0]

        print(content)
        contentArr = content.split("\t")
        command = str(contentArr[0])#+"(*args)"
        argCountStr = contentArr[1]
        argCount = int(argCountStr)

        for index in range(argCount):
            cmdArgs.append(contentArr[2+index])    
    return command, cmdArgs

def packer(content):
    package = Messages.ariaHead+content+Messages.ariaTail
    return package

def commandSelect(command, argList):
    errorId = -1

    print("command: "+command)

    match command:

        # Errors .............................................................................................................
        # In these functions we read an (int) errorId (-1 if none)
        
        # string errorMsg
        # Error severity (enum type)

        case "Error.GetErrorMessage":
            # (string) message associated to the error ID provided. argList: [(int) errorId]
            errorMsg = Error.GetErrorMessage(*argList) 
            return "message"+errorMsg
        
        case "Error.GetErrorSeverity":            
            # Enum type ErrorSeverity for the error with ID provided. argList: [(int) errorId]
            severity = Error.GetErrorSeverity(*argList)
            return "value"+str(severity)
        
        case "Error.GetErrorTimestamp":
            # string timestamp (format: yyyy/MM/dd-HH:mm:ss) of the error. argList: [(int) errorId]
            errorTimestamp = Error.GetErrorTimestamp(*argList) # string errorTimestamp (formatted as yyyy/MM/dd-HH:mm:ss)
            return "message"+errorTimestamp
        
        case "Error.ResetErrors":
            # Clears the error stack of all previous errors. Next error ID will be then 0.
            errorId = Error.ResetErrors()
            return "value"+str(errorId)
        
        case "Error.TryGetNextAsyncError":
            # ID of the oldest error from the stack.
            errorId = Error.TryGetNextAsyncError() 
            return "value"+str(errorId)
        
        # Instrument..........................................................................................................
        #argList = [FlowUnitType flowUnit , SwitchType externalSwitch]; e.g. [Enums.FlowUnitType.L, Enums.SwitchType.MSwitch]
        # Enums.FlowUnitType.L (liters)
        # Enums.SwitchType.MSwitch (m-switch valve)

        case "Instrument.LoadPhysicalInstrument":
            # Searches for a connected Aria instrument and loads it. Returns true if a connected Aria instrument was detected, false otherwise.
            success, errorId = Instrument.LoadPhysicalInstrument(*argList)
            if errorId == -1:
                return "message"+success
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
            
        case "Instrument.LoadSimulatedInstrument":
            # Loads a Simulated Instrument with the given flowUnit and externalSwitch types
            errorId = Instrument.LoadSimulatedInstrument(*argList)
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
        # FLow control........................................................................................................        
        case "FlowControl.SetPressureOrder":
            # "Set pressure" order to pressure (in mBar). argList: [(float) pressure]
            errorId = FlowControl.SetPressureOrder(*argList)
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
        
        case "FlowControl.GetMeasuredPressure":
            # Current pressure measured in mBar
            currentPressure, errorId = FlowControl.GetMeasuredPressure() # currentPressure (float)
            if errorId == -1:
                return "value"+str(currentPressure)
            else:
                return "error"+ Error.GetErrorMessage(errorId) 

        case "FlowControl.GetMeasuredFlowRate":
            # Current flowrate in µl/min
            currentFlowrate, errorId = FlowControl.GetMeasuredFlowRate() # float currentFlowrate
            if errorId == -1:
                return "value"+str(currentFlowrate)
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
            
        # Sequence............................................................................................................        
        case "Sequence.EnablePrefill":
            # Enables or disables the prefill phase at the begining of the sequence. argList: [(bool) enable]
            errorId = Sequence.EnablePrefill(*argList)
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId)
            
        case "Sequence.StartSequence":
            # Start the current sequence.
            errorId = Sequence.StartSequence()
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId)
            
        # Configuration.......................................................................................................
        case "Configuration.SetPrefillAndPreloadFlowRate":
            # Defines the prefill and preload flowrate from the given FlowratePreset. 
            # argList: [enum lowratePreset *flowratePreset], e.g. FlowRatePreset.Max
            errorId = Configuration.SetPrefillAndPreloadFlowRate(*argList)
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId)
            
        # Step list...........................................................................................................                    
        case "StepList.InsertTimedInjectionStep":
            # Inserts a step of timed injection in the current sequence at index. 
            # argList: [(int) index , (int) inputReservoir, (int) destination, (float) flowRate(in µl/min), (int) duration_s, 
            # (bool) preSignal, (SignalType) preSignalType, (bool) postSignal, (SignalType) postSignalType ]
            # e.g. argList = [-1, 1, 1, 200, 60, False, SignalType.TTL, False, SignalType.TTL]
            errorId = StepList.InsertTimedInjectionStep(*argList)
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
            
        case "StepList.InsertVolumeInjectionStep":
            # Inserts a step of volume injection in the current sequence at index. 

            # argList: [(int) index , (int) inputReservoir, (int) destination, (float) flowRate(in µl/min), (float) volume(in µl), 
            # (bool) preSignal, (SignalType) preSignalType, (bool) postSignal, (SignalType) postSignalType ]
            # e.g. argList = -1, 1, 1, 500, 100, True, SignalType.TTL, False, SignalType.TTL]

            errorId = StepList.InsertVolumeInjectionStep(*argList)
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
                        
        case "StepList.InsertWaitUserStep":
            # Inserts a step of waiting for user in the current sequence at index. argList: [(int) index , (int) timeout_s, 
            # (bool) preSignal, (SignalType) preSignalType, (bool) postSignal, (SignalType) postSignalType ]
            # e.g. argList = -1, 3600, False, SignalType.TTL, False, SignalType.TTL]
            errorId = StepList.InsertWaitUserStep(*argList)
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
            
        # Monitoring..........................................................................................................
        case "Monitoring.IsSequenceRunning":
            # True if the current sequence is in progress, false otherwise. A paused sequence is still considered as running.
            running, errorId = Monitoring.IsSequenceRunning()
            if errorId == -1:
                return "value"+str(running)
            else:
                return "error"+ Error.GetErrorMessage(errorId) 

        case "Monitoring.PauseSequence":
            # Pauses the current sequence execution.
            errorId = Monitoring.PauseSequence()
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
            
        case "Monitoring.IsSequencePaused":
            # True if the sequence is paused, false otherwise.
            paused, errorId = Monitoring.IsSequencePaused()
            if errorId == -1:
                return "value"+str(paused)
            else:
                return "error "+ Error.GetErrorMessage(errorId)
            
        case "Monitoring.ResumeSequenceExecution":
            # Resumes the execution of a paused sequence.
            errorId = Monitoring.ResumeSequenceExecution()
            if errorId == -1:
                return "message"+"OK"
            else:
                return "error "+ Error.GetErrorMessage(errorId)

        case "Monitoring.GetPrefillStepNumber":
            # Step index (int) of the Prefill phase of the sequence.
            prefillStepNumber = Monitoring.GetPrefillStepNumber()
            if errorId == -1:
                return "value"+str(prefillStepNumber)
            else:
                return "error"+ Error.GetErrorMessage(errorId) 

        case "Monitoring.GetPreloadStepNumber":
            # Step index (int) of the Preload phase of the sequence.            
            preloadStepNumber = Monitoring.GetPreloadStepNumber()
            if errorId == -1:
                return "value"+str(preloadStepNumber)
            else:
                return "error"+ Error.GetErrorMessage(errorId) 
            
        case "Monitoring.GetCurrentStep":
            # Step index (int) of the current step of the sequence.
            currentStep, errorId = Monitoring.GetCurrentStep()
            if errorId == -1:
                return "value"+str(currentStep)
            else:
                return "error"+ Error.GetErrorMessage(errorId)             

        case "Monitoring.GetProgress":
            # Progress level (float, in %)  of the step at index stepId.
            # argList: [(int) stepId] 
            progress, errorId = Monitoring.GetProgress(*argList)
            if errorId == -1:
                return "value"+str(progress)
            else:
                return "error"+ Error.GetErrorMessage(errorId)
                                   
        case "Monitoring.GetPrefillAndPreloadProgress":
            # Cumulated progress (float) for the Prefill and Preload phases.
            progress, errorId = Monitoring.GetPrefillAndPreloadProgress()
            if errorId == -1:
                return "value"+str(progress)
            else:
                return "error"+ Error.GetErrorMessage(errorId)
            
        case "Monitoring.HasSequenceEnded":
            # Returns true if the sequence execution ended
            finished, errorId = Monitoring.HasSequenceEnded()
            if errorId == -1:
                return "message"+finished
            else:
                return "error"+ Error.GetErrorMessage(errorId)
            
    return "no_match"
main()