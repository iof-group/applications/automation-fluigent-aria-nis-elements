// Copyright 2023 Nasser Darwish
// Institute of Science and Technology Austria
// GNU AFFERO GENERAL PUBLIC LICENSE, Version 3

/********************************************************************
Build and shape the interface window 
********************************************************************/
#define EM_FMTLINES 200
#define EM_SETSEL 177
#define EM_REPLACESEL 194
#define WM_NCPAINT 133

int main(){

   // Window handles
   global char nis2py_title[64] = "NIS 2 <-> PY";
   global long hNis;
   global long hDlg;
   global long hCtrl;  
   global int msgLineLngth;
   global int lblWdth;

   return 0;
}

int nis2py_showDialog(){

	msgLineLngth = 210;
	lblWdth = 80;
	
   if(!ExistProc("NkDialog_Create"))
   {
      RunMacro("c:/Program Files/NIS-Elements/Macros/Win32.mac");
      RunMacro("c:/Program Files/NIS-Elements/Macros/NkWindow.mac");
   }

   NkWindow_GetMainWindow("NIS-Elements","lxMDIClient",&hNis);
   if(hNis == 0){ 
      Int_Question(nis2py_title,"Failed to find NIS window","Close","","","",0,0); 
      return 0; 
   }

   NkDialog_Create(hNis,nis2py_title,&hDlg);
   NkDialog_AddMacroMessageHandler(hDlg,"",16,0,0,0,0,"nis2py_OnClose()");
                        
   // Label column
   NkDialog_AddColumn(hDlg);
 
   // Log messages:

   NkDialog_AddControl(hDlg,"consoleMsgBox","edit",    WS_VSCROLL+WS_TABSTOP+ES_MULTILINE+ES_AUTOVSCROLL,-12,msgLineLngth,&hCtrl); 
   
   
   
   // Fluidic command combo box
   addCommandCombo();
   
   
   // The "send" button must appear for any command chosen:
   NkDialog_AddControl(hDlg,"btnSend","button",BS_PUSHBUTTON,0,90,&hCtrl);
   NkWindow_SendMessageString(hCtrl,WM_SETTEXT,0,"send");

   // The command here must assemble parameters in the string and send them
   NkDialog_AddMacroMessageHandler(hDlg,"btnSend",WM_COMMAND,0,0,0,0,"requestSelectedCommand();");

   // "End application" button
   NkDialog_AddControl(hDlg,"btnEnd","button",BS_PUSHBUTTON,0,90,&hCtrl);
   NkWindow_SendMessageString(hCtrl,WM_SETTEXT,0,"Close app");

   // The command here must assemble parameters in the string and send them
   NkDialog_AddMacroMessageHandler(hDlg,"btnEnd",WM_COMMAND,0,0,0,0,"serverRequest(&SKT_KILL);");


      
   NkWindow_ShowWindow(hDlg,0);
   NkWindow_ShowWindow(hDlg,1);
   
   
   NkWindow_ShowWindow(hDlg,1);
   //nis2py_PositionDialog();
   //nis2py_busy = 0;
}







// This is what happens when the "send" button is pressed:
int requestSelectedCommand(){

   appendMessage(packet);

   if (strcmp(packet,"")!=0 ){
         srvRPLY = serverRequest(packet);
   }   
}



int addCommandCombo(){
   int i;
   char buf[256];
   int commands[2048];
   char titles[64][100];
   char tmpStr[64];
   int64 map[4];
   char* init;
     
   memcpy(&titles+(64*0), "Error.GetErrorMessage",64);
   memcpy(&titles+(64*1), "Error.GetErrorSeverity",64);
   memcpy(&titles+(64*2), "Error.GetErrorTimestamp",64);
   memcpy(&titles+(64*3), "Error.ResetErrors",64);
   memcpy(&titles+(64*4), "Error.TryGetNextAsyncError",64);   
   memcpy(&titles+(64*5), "Instrument.LoadPhysicalInstrument",64);      
   memcpy(&titles+(64*6), "Instrument.LoadSimulatedInstrument",64);
   memcpy(&titles+(64*7), "FlowControl.SetPressureOrder",64);  
   memcpy(&titles+(64*8), "FlowControl.GetMeasuredPressure",64);
   memcpy(&titles+(64*9), "FlowControl.GetMeasuredFlowRate",64);  
   memcpy(&titles+(64*10),"Sequence.EnablePrefill",64);
   memcpy(&titles+(64*11),"Sequence.StartSequence",64);
   memcpy(&titles+(64*12),"Configuration.SetPrefillAndPreloadFlowRate",64);
   memcpy(&titles+(64*13),"StepList.InsertTimedInjectionStep",64);
   memcpy(&titles+(64*14),"StepList.InsertVolumeInjectionStep",64);
   memcpy(&titles+(64*15),"StepList.InsertWaitUserStep",64);
   memcpy(&titles+(64*16),"Monitoring.IsSequenceRunning",64);
   memcpy(&titles+(64*17),"Monitoring.PauseSequence",64);
   memcpy(&titles+(64*18),"Monitoring.IsSequencePaused",64);
   memcpy(&titles+(64*19),"Monitoring.ResumeSequenceExecution",64);
   memcpy(&titles+(64*20),"Monitoring.GetPrefillStepNumber",64);
   memcpy(&titles+(64*21),"Monitoring.GetPreloadStepNumber",64);
   memcpy(&titles+(64*22),"Monitoring.GetCurrentStep",64);
   memcpy(&titles+(64*23),"Monitoring.GetProgress",64);
   memcpy(&titles+(64*24),"Monitoring.GetPrefillAndPreloadProgress",64);
   memcpy(&titles+(64*25),"Monitoring.HasSequenceEnded",64);
   
   NkDialog_AddControl(hDlg,"lblCombobox","static",SS_SIMPLE,0,80,&hCtrl);
   NkWindow_SendMessageString(hCtrl,WM_SETTEXT,0,"combobox");
   NkDialog_AddControl(hDlg,"ctlCombobox","combobox",WS_TABSTOP+CBS_DROPDOWNLIST,0,150,&hCtrl);   
   for(i = 1; i < 26; i = i + 1){
      memcpy(buf,titles[0]+(64*(i-1)),64);
      NkWindow_SendMessageString(hCtrl,CB_ADDSTRING,0,buf);
   }
   NkWindow_SendMessageString(hCtrl,CB_SETCURSEL,0,0); // select 'item 0'
   NkDialog_AddMacroMessageHandler(hDlg,"ctlCombobox",WM_COMMAND,CBN_SELENDOK,0,0,0,"NKWD_OnCombobox();");      
}

int NKWD_OnCombobox(){

   long iCurSel;
   char selCmd[256];
   char paramStr[256];   
   char buf[256];


   packet = "";




   NkDialog_FindControl(hDlg,"ctlCombobox",&hCtrl);
   iCurSel = NkWindow_SendMessage(hCtrl,CB_GETCURSEL,0,0);
   NkWindow_SendMessageString(hCtrl,CB_GETLBTEXT,iCurSel,selCmd);
   sprintf(buf,"new combo select: %d: %s%c%c","iCurSel,selCmd,13,10");

   if (selCmd == "Error.GetErrorMessage"){
  
      // Replace these dialog boxes with the appropriate parameters
      NkDialog_AddControl(hDlg,"ctlLsEdit","NkLsEdit",WS_TABSTOP+ES_RIGHT+ES_MULTILINE+ES_AUTOHSCROLL+ES_AUTOVSCROLL+ES_WANTRETURN,-1,50,&hCtrl);
      NkLsEdit_SetRange(hCtrl,0.01,100.0,0.01); // lower limit 0.01, higher limit 100.0, increment 0.01
      NkLsEdit_SetFormat(hCtrl,"%.2lf"); // printf format to display the value
      NkLsEdit_SetUnit(hCtrl," %"); // unit added to the number
      NkLsEdit_SetValue(hCtrl,42.42); 

      NkWindow_ShowWindow(hDlg,0);
      NkWindow_ShowWindow(hDlg,1);
   }
   if (selCmd == "Error.GetErrorSeverity"){

      //arguments[0] = 1; //"errorId"
      //type01 = "int";
   
   }
   if (selCmd == "Error.GetErrorTimestamp"){
   
      //arguments[0]    = 1; //"errorId"
      //type01     = "int";   
   
   }
   if (selCmd == "Error.ResetErrors"){
      paramStr  = "";
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Error.TryGetNextAsyncError"){
	  paramStr  = "";
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Instrument.LoadPhysicalInstrument"){      
      paramStr  = "Enums.FlowUnitType.L,Enums.SwitchType.MSwitch";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Instrument.LoadSimulatedInstrument"){ 
      paramStr  = "Enums.FlowUnitType.L,Enums.SwitchType.MSwitch";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "FlowControl.SetPressureOrder"){   
   
      //argument01 = "pressure";
      //type01     = "double";      
   }
   if (selCmd == "FlowControl.GetMeasuredPressure"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "FlowControl.GetMeasuredFlowRate"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Sequence.EnablePrefill"){
   
      //argument01 = "bool enable";
      //type01     = "int";      
   }
   if (selCmd == "Sequence.StartSequence"){
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);   
   }
   if (selCmd == "Configuration.SetPrefillAndPreloadFlowRate"){  
   
      //argument01 ="flowratePreset";
      //type01     = "char*";      
   }
   if (selCmd == "StepList.InsertTimedInjectionStep"){   
   /*
   
   

   
      argument01 = "index";
      argument02 = "inputReservoir";
      argument03 = "destination";
      argument04 = "flowRate_ulmin";
      argument05 = "duration_s";
      argument06 = "preSignal";
      argument07 = "preSignalType";
      argument08 = "postSignal";
      argument09 = "postSignalType";
      
      type01 = "int";
      type02 = "int";
      type03 = "int";
      type04 = "double";
      type05 = "int";
      type06 = "int";
      type07 = "char*";
      type08 = "int";
      type09 = "char*";
   
   
   
   
   
   */
   
   
   }
   if (selCmd == "StepList.InsertVolumeInjectionStep"){
   
   /*
   
   
   
   
      
      argument01 = "index";
      argument02 = "inputReservoir";
      argument03 = "destination";
      argument04 = "flowRate_ulmin";
      argument05 = "volume_ul";
      argument06 = "preSignal";
      argument07 = "preSignalType";
      argument08 = "postSignal";
      argument09 = "postSignalType";
      
      type01 = "int";
      type02 = "int";
      type03 = "int";
      type04 = "double";
      type05 = "double";
      type06 = "int";
      type07 = "char*";
      type08 = "int";
      type09 = "char*";
   
   
   */
   
   
   }
   if (selCmd == "StepList.InsertWaitUserStep"){ 
   
   /*
   
   
   
   
   
   
   
      argument01 = "index";
      argument02 = "timeout_s";
      
      argument03 = "preSignal";
      argument04 = "preSignalType";
      argument05 = "postSignal";
      argument06 = "postSignalType";
      
      type01 = "int";
      type02 = "int";

      type03 = "int";
      type04 = "char*";
      type05 = "int";
      type06 = "char*";      
   
   */
   }
   if (selCmd == "Monitoring.IsSequenceRunning"){
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.PauseSequence"){
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.IsSequencePaused"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.ResumeSequenceExecution"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.GetPrefillStepNumber"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.GetPreloadStepNumber"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.GetCurrentStep"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.GetProgress"){   
   
//argument01 = "intstepId";
//type01     = "int";

   }
   if (selCmd == "Monitoring.GetPrefillAndPreloadProgress"){   
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }
   if (selCmd == "Monitoring.HasSequenceEnded"){
      paramStr  = "";  
      packet = packMessage(selCmd, paramStr);
   }   
   




   

   
}



int nis2py_PositionDialog(){

   int dt_left;
   int dt_top;
   int dt_width;
   int dt_height; 
   int dlg_x_max;
   int dlg_y_max;
   int dlg_width;
   int dlg_height;
   
   dt_left = GetSystemMetrics(SM_XVIRTUALSCREEN);
   dt_top =  GetSystemMetrics(SM_YVIRTUALSCREEN);
   dt_width = GetSystemMetrics(SM_CXVIRTUALSCREEN);
   dt_height = GetSystemMetrics(SM_CYVIRTUALSCREEN);
   NkWindow_GetWindowSize(hDlg,&dlg_width,&dlg_height);
   if((nis2py_dlg_top == -1) || (nis2py_dlg_left == -1))
   {
      nis2py_dlg_left = dt_left + (2*(dt_width  - dlg_width ) / 3);
      nis2py_dlg_top  = dt_top  + ((dt_height - dlg_height) / 2);
   }
   else
   {
      if(nis2py_dlg_left < dt_left) { nis2py_dlg_left = dt_left; }
      if(nis2py_dlg_top  < dt_top ) { nis2py_dlg_top  = dt_top; }
      dlg_x_max = dt_left + dt_width - dlg_width;
      dlg_y_max = dt_top + dt_height - dlg_height;
      if(nis2py_dlg_left > dlg_x_max) { nis2py_dlg_left  = dlg_x_max; }
      if(nis2py_dlg_top  > dlg_y_max) { nis2py_dlg_top   = dlg_y_max; }
   }
   NkWindow_SetWindowPos(hDlg,nis2py_dlg_left,nis2py_dlg_top);
}

/********************************************************************
Auxiliary functions 
********************************************************************/

int addLinesBelow(long hDlg, long hCtrl, int lineNumber){
   // iserts the given number of lines below on the current window
   int lineInd;
   for(lineInd =0; lineInd<lineNumber; ++lineInd){
      NkDialog_AddControl(hDlg,"voidLine","static",SS_SIMPLE,0,120,&hCtrl);
      //NkWindow_SendMessageString(hCtrl,WM_SETTEXT,0,"");
      NkDialog_AddRow(hDlg);
   }   
   return 0;
}

int displayMessage(char *consoleMsg){
   // Adds the given message on the virtual console
   char consoleBuf[16384];      
   int TextLen;
   int charInd;
   //int lineCharWidth = 63;
   
   strcat(consoleBuf," ");
   strcat(consoleBuf,consoleMsg);
      
   NkDialog_FindControl(hDlg,"consoleMsgBox",&hCtrl);
   TextLen = NkWindow_SendMessageString(hCtrl, WM_GETTEXTLENGTH, 0, 0);
   NkWindow_SendMessageString(hCtrl, EM_SETSEL, TextLen, TextLen);
   NkWindow_SendMessageString(hCtrl, EM_REPLACESEL, FALSE, consoleBuf);
}

int appendMessage(char *consoleMsg){
   // Adds the given message on the virtual console
   char consoleBuf[16384];
   int TextLen;
   int charInd;
   int lineCharWidth = 63;
   
   strcat(consoleBuf,consoleMsg);
   // This is how we add line breaks:
   sprintf(consoleBuf,"%s %c%c","consoleMsg,13,10");
   NkDialog_FindControl(hDlg,"consoleMsgBox",&hCtrl);
   TextLen = NkWindow_SendMessageString(hCtrl, WM_GETTEXTLENGTH, 0, 0);   
   
   NkWindow_SendMessageString(hCtrl, EM_SETSEL, TextLen, TextLen);   
   NkWindow_SendMessageString(hCtrl, EM_REPLACESEL, FALSE, consoleBuf);
   NkWindow_SendMessage(hCtrl,EM_SCROLLCARET,0,0);
}

int nis2py_OnClose(){
   global long hDlg;
   int result;
   
   result = 0;
   serverRequest(&SKT_KILL);
   NkWindow_DestroyWindow(hDlg);
   return result;
}

int loadDefaultValues()
{
   char csvBuffer[4096];

   // mouse
   keys[0]  = VK_LBUTTON;
   keys[1]  = VK_RBUTTON;
   keys[2]  = VK_LBUTTONDBLCLK; // Primary mouse button double click
   keys[3]  = VK_RBUTTONDBLCLK; // Secondary mouse button double click
   
   // keyboard
   keys[4]  = VK_RETURN;
   keys[5]  = VK_ESC;
   
   // direction
   keys[6]  = VK_UP;       // Up key
   keys[7]  = VK_DOWN;     // Down key
   keys[8]  = VK_LEFT;     // Left key
   keys[9]  = VK_RIGHT;    // Right key
   
   // back
   keys[10] = VK_BACK;     // BackSpace key
   // Spacebar
   //keys[10] = VK_SPACE; // Spacebar
   
   // page
   keys[11]  = VK_PGUP;     // PgUp key
   keys[12]  = VK_PGDN;     // PgDn key
   
   // ends
   keys[13] = VK_HOME;      // Home key   
   keys[14] = VK_END;       //End key
   
   // modifiers
   keys[15] = VK_SHIFT;
   keys[16] = VK_CTRL;
   keys[17] = VK_ALT;
   keys[18] = VK_TAB;      // Tab key
   // operators
   keys[19] = VK_ADD;      // + key
   keys[20] = VK_SUBTRACT; // - key   
   
   CUT_OFFLINE_HEADER = "cutter_cut_from_file";
   CUT_HEADER = "cutter_points_start:";
   CUT_TAIL = ":cutter_points_ended";
   POWER_STRING = "cutter_set_power";
   DENSITY_STRING = "cutter_set_density";
   FREQ_STRING = "cutter_set_frequency";
   POINTS_STRING = "cutter_set_points_per_shot";
   SHAPE_STRING = "cutter_set_path_shape";
   PIX_STRING = "cutter_set_pixel_size";   
   CLOSE_REQUEST = "cutter_end_connection";
   INIT_REQUEST = "cutter_start_scanner";
   CONNECTION_SUCCESSFUL = "connection_successful";
   FILE_STATUS_REQUEST   = "cutter_report_file";
   KERNEL_READY = "cutter_kernel_ready";   

   OPERATION_SUCCESSFULL = "cutter_op_success";
   HW_ERROR = "cutter_HW_ERROR";
   MSG_CORRUPT = "cutter_MSG_CORRUPT";
   SCANNER_OK = "cutter_scanner_rdy";
   SCANNER_ERROR = "cutter_scanner_error";
   CUT_COMPLETED = "cutter_cut_completed";
   INFO_UPDATED = "cutter_info_updated";
   CUTTER_CONNECTED = "cutter_dev_connected";
   CUTTER_DISCONNECTED = "cutter_dev_disconnected";
   FILE_OK = "cutter_file_read";
   FILE_ERROR = "cutter_file_error";      
   CUT_NOW_REQUEST = "cutter_proceed_cut";   
   
   return 0;
}
 